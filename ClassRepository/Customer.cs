﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRepository
{
    public class Customer
    {
        public long ID { get; set; }
        public string GUID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int NetworkId { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public Customer()
        {
            this.Name = "";
            this.ID = 0;
            this.LastName = "";
            this.NetworkId = 0;
            this.GUID = "";
            this.Email = "";
            this.Telephone = "";
        }
    }
    public class CustomerAPI
    {
        public ResponseStatus Status { get; set; }
        public List<Customer> Customers { get; set; }
        public CustomerAPI()
        {
            this.Customers = new List<ClassRepository.Customer>();
            this.Status = new ClassRepository.ResponseStatus();
        }
    }
}
