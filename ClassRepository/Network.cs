﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassRepository
{
    public class Network
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PreName { get; set; }
        public Network()
        {
            this.Id = 0;
            this.Name = "";
            this.PreName = "";
        }

    }
}
