﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRepository
{
    public class VAT
    {
        public int Id { get; set; }
        public string GUID { get; set; }
        public string Code { get; set; }
        public int NetworkId { get; set; }
        public string Name { get; set; }
        public string DescriptionInRow { get; set; }
        public decimal Value { get; set; }
        public VAT()
        {
            this.Code = "";
            this.DescriptionInRow = "";
            this.GUID = "";
            this.Id = 0;
            this.Name = "";
            this.NetworkId = 0;
            this.Value = 0;
        }

    }
}
