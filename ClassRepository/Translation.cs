﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRepository
{
    public class LabelTranslation
    {
        public string Key { get; set; }
        public string Language { get; set; }
        public string Context { get; set; }
        public string Translation { get; set; }
        public string TranslationExtension { get; set; }
        public LabelTranslation()
        {
            this.Context = "";
            this.Key = "";
            this.Language = "";
            this.Translation = "";
            this.TranslationExtension = "";
        }
    }
    public class LabelTranslationExt : LabelTranslation
    {
        public long Id { get; set; }
        public int LangId { get; set; }
        public LabelTranslationExt() : base()
        {
            this.Id = 0;
            this.LangId = 0;
        }
    }
    public class Lang
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Lang()
        {
            this.Code = "en-GB";
            this.Id = 2;
            this.Name = "English";
        }
    }
    public class LangNonInUse
    {
        public string Context { get; set; }
        public string Key { get; set; }
        public LangNonInUse()
        {
            this.Context = "";
            this.Key = "";
        }
    }
}
