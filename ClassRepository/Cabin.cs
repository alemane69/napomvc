﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRepository
{
    public class Cabin
    {
        public long ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int NetworkId { get; set; }
        public Cabin()
        {
            this.Code = "";
            this.ID = 0;
            this.Name = "";
            this.NetworkId = 0;
        }
    }
}
