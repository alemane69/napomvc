﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRepository
{
    public class Service
    {
        public string GUID { get; set; }
        public int Minutes { get; set; }
        public int WorkingMinutes { get; set; }
        public long ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int NetworkId { get; set; }
        public decimal Price { get; set; }
        public int VAT { get; set; }
        public decimal ValueVAT { get; set; }
        //public ServiceTypesBase ServiceType1 { get; set; }
        //public ServiceTypesBase ServiceType2 { get; set; }
        public ServiceTypeComplex ServiceType { get; set; }
        public List<ServicePrices> SaleListPrices { get; set; }
        public int type1 { get; set; }
        public int type2 { get; set; }
        public Service()
        {
            this.Code = "";
            this.ID = 0;
            this.Name = "";
            this.NetworkId = 0;
            this.GUID = "";
            this.Minutes = 0;
            //this.ServiceType1 = new ServiceTypesBase();
            //this.ServiceType2 = new ServiceTypesBase();
            this.Price = 0;
            this.WorkingMinutes = 0;
            this.VAT = 0;
            this.SaleListPrices = new List<ServicePrices>();
            this.ValueVAT = 0;
            this.type1 = 0;
            this.type2 = 0;
        }
    }
    public class ServiceTypeComplex
    {
        public ServiceTypesBase ServiceTypeParent { get; set; }
        public ServiceTypesBase ServiceTypeChild { get; set; }
        public ServiceTypeComplex()
        {
            this.ServiceTypeChild = new ClassRepository.ServiceTypesBase();
            this.ServiceTypeParent = new ClassRepository.ServiceTypesBase();
        }

    }

    public class ServicePrices
    {
        public int SaleListId { get; set; }
        public decimal Price { get; set; }
        public ServicePrices()
        {
            this.Price = 0;
            this.SaleListId = 0;
        }

    }
    public class ServiceGrid
    {
        public string GUID { get; set; }
        public int Minutes { get; set; }
        public int WorkingMinutes { get; set; }
        public long ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int NetworkId { get; set; }
        public decimal Price { get; set; }
        public decimal VAT { get; set; }
        public int VATId { get; set; }
        public int ServiceTypeId1 { get; set; }
        public string ServiceTypeName1 { get; set; }
        public int ServiceTypeId2 { get; set; }
        public string ServiceTypeName2 { get; set; }
        public List<ServicePrices> SaleListPrices { get; set; }
        public ServiceGrid()
        {
            this.Code = "";
            this.ID = 0;
            this.Name = "";
            this.NetworkId = 0;
            this.GUID = "";
            this.Minutes = 0;
            this.ServiceTypeId1 = 0;
            this.ServiceTypeName1 = "";
            this.ServiceTypeId2 = 0;
            this.ServiceTypeName2 = "";
            this.Price = 0;
            this.WorkingMinutes = 0;
            this.VAT = 0;
            this.VATId = 0;
            this.SaleListPrices = new List<ServicePrices>();
        }
    }
    public class ServiceAPI
    {
        public ResponseStatus Status { get; set; }
        public List<Service> Services { get; set; }
        public ServiceAPI()
        {
            this.Status = new ClassRepository.ResponseStatus();
            this.Services = new List<ClassRepository.Service>();
        }
    }

    public class ServiceTypesBase
    {
        public int Id { get; set; }
        public string GUID { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public int NetworkId { get; set; }
        public ServiceTypesBase()
        {
            this.Id = 0;
            this.Name = "";
            this.GUID = "";
            this.NetworkId = 0;
            this.ParentName = "";
        }
    }
    public class ServiceBaseResponse
    {
        public ResponseStatus Status { get; set; }
        public Service Type { get; set; }
        public ServiceBaseResponse()
        {
            this.Status = new ClassRepository.ResponseStatus();
            this.Type = new ClassRepository.Service();
        }
    }
    public class ServiceTypeBaseResponse
    {
        public ResponseStatus Status { get; set; }
        public ServiceTypesBase Type { get; set; }
        public ServiceTypeBaseResponse()
        {
            this.Status = new ClassRepository.ResponseStatus();
            this.Type = new ClassRepository.ServiceTypesBase();
        }
    }
    public class TypeComplete
    {
        public ServiceTypesBase ServiceType { get; set; }
        public List<ServiceTypesBase> ServiceTypeChildren { get; set; }
    }

    public class ServiceTypeAPI
    {
        public ResponseStatus Status { get; set; }
        public List<TypeComplete> ServiceTypes { get; set; }
        public ServiceTypeAPI()
        {
            this.Status = new ResponseStatus();
            this.ServiceTypes = new List<TypeComplete>();
        }
    }

    public class SaleList
    {
        public long Id { get; set; }
        public string GUID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public SaleList()
        {
            this.Id = 0;
            this.GUID = "";
            this.Name = "";
            this.Note = "";
            this.Code = "";
        }
    }

}
