﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRepository
{
    public class Employee
    {
        public int ID { get; set; }
        public string GUID { get; set; }
        public string Code { get; set; }
        public int NetworkId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int UserId { get; set;}
        public string BirthPlace { get; set; }
        public string BirthNation { get; set; }
        public string ResidenceAdress { get; set; }
        public string CityAdress { get; set; }
        public string ZipAdress { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public int SaleList { get; set; }
        public bool Obsolete { get; set; }

        public Employee()
        {
            this.GUID = "";
            this.ID = 0;
            this.NetworkId = 0;
            this.Name = "";
            this.LastName = "";
            this.BirthNation = "";
            this.BirthPlace = "";
            this.CityAdress = "";
            this.Email = "";
            this.Obsolete = false;
            this.ResidenceAdress = "";
            this.SaleList = 0;
            this.Telephone = "";
            this.UserId = 0;
            this.ZipAdress = "";
            this.Code = "";
        }
    }
    public class EmployeeAPI
    {
        public ResponseStatus Status { get; set; }
        public List<Employee> Employees { get; set; }
        public EmployeeAPI()
        {
            this.Employees = new List<ClassRepository.Employee>();
            this.Status = new ClassRepository.ResponseStatus();
        }

    }
}
