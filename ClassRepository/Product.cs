﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRepository
{
    public class Product
    {
        public long ID { get; set; }
        public string GUID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int NetworkId { get; set; }
        public Product()
        {
            this.Code = "";
            this.ID = 0;
            this.Name = "";
            this.NetworkId = 0;
            this.GUID = "";
            this.Price = 0;
        }
    }
    public class ProductAPI
    {
        public ResponseStatus Status { get; set; }
        public List<Product> Products { get; set; }
        public ProductAPI()
        {
            this.Products = new List<ClassRepository.Product>();
            this.Status = new ClassRepository.ResponseStatus();
        }
    }
}
