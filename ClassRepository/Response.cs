﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRepository
{
    public class ResponseStatus
    {
        public int Status { get; set; }
        public string Error { get; set; }
        public ResponseStatus()
        {
            this.Error = "";
            this.Status = 0;
        }
    }
}
