﻿using System.Web;
using System.Web.Optimization;

namespace NapoMVC
{
    public class BundleConfig
    {
        // Per ulteriori informazioni sulla creazione di bundle, visitare http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilizzare la versione di sviluppo di Modernizr per eseguire attività di sviluppo e formazione. Successivamente, quando si è
            // pronti per passare alla produzione, utilizzare lo strumento di compilazione disponibile all'indirizzo http://modernizr.com per selezionare solo i test necessari.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/coreplugin").Include(
                      "~/Assets/global/plugins/bootstrap/js/bootstrap.min.js",
                      "~/Assets/global/plugins/js.cookie.min.js",
                      "~/Assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                      "~/Assets/global/plugins/jquery.blockui.min.js",
                      "~/Assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
                      "~/Assets/global/scripts/app.min.js",
                      "~/Assets/kendo/2017.1.223/js/kendo.all.min.js",
                      "~/Assets/kendo/2017.1.223/js/kendo.aspnetmvc.min.js"));
        }
    }
}
