﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassRepository;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.OAuth;
using NapoMVC.Models.BLL;
namespace NapoMVC.Areas.API.Controllers
{
    [RoutePrefix("api/EmployeesAPI")]
    public class EmployeesApiController : ApiController
    {

        [Authorize]
        [Route("")]
        public IHttpActionResult Get()

        {
            HttpClient _client = new HttpClient();
            string _baseAddrs = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            Uri _uri = new Uri(_baseAddrs);
            //var tokenResponse = _client.PostAsync(_client.BaseAddress + "accesstoken", new FormUrlEncodedContent(form)).Result;
            var tokenResponse = Request.Headers.Authorization.Parameter;
            
            OAuthGrantResourceOwnerCredentialsContext _ctx;
            string _userName = HttpContext.Current.User.Identity.Name;
            UserBLL oUserBLL = new Models.BLL.UserBLL();
            int NetworkId = oUserBLL.getUserNetwork(User.Identity.Name);
            NetworkId = 1;
            EmployeeAPI oRet = new EmployeeAPI();
            EmployeeBLL oEmployeeBLL = new Models.BLL.EmployeeBLL();
            oRet = oEmployeeBLL.getEmployeesInNetworkAPI(NetworkId);
            return Ok(oRet);
        }
        public IHttpActionResult Get(int NetworkId)
        {
            string _userName = HttpContext.Current.User.Identity.Name;
            UserBLL oUserBLL = new Models.BLL.UserBLL();
            //int NetworkId = oUserBLL.getUserNetwork(User.Identity.Name);
            EmployeeAPI oRet = new EmployeeAPI();
            EmployeeBLL oEmployeeBLL = new Models.BLL.EmployeeBLL();
            oRet = oEmployeeBLL.getEmployeesInNetworkAPI(NetworkId);
            return Ok(oRet);
        }
        //// GET: api/EmployeesApi
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/EmployeesApi/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/EmployeesApi
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/EmployeesApi/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/EmployeesApi/5
        //public void Delete(int id)
        //{
        //}
    }
}
