﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using ClassRepository;
using NapoMVC.Models.BLL;
namespace NapoMVC.Areas.API.Controllers
{
    public class CustomersController : Controller
    {
        // GET: API/Customers
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult getCustomersPerNetwork(int NetworkId,string Token)
        {
            //verifica del token


            //verifica passata
            CustomerAPI oRet = new CustomerAPI();
            CustomerBLL oCustomerBLL = new Models.BLL.CustomerBLL();
            oRet = oCustomerBLL.getCustomerPerNetwork_test(NetworkId);
            return Json(oRet, JsonRequestBehavior.AllowGet);
        }
    }
}