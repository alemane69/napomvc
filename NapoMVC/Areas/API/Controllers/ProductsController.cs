﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClassRepository;
using NapoMVC.Models.BLL;
using System.Web.Mvc;

namespace NapoMVC.Areas.API.Controllers
{
    public class ProductsController : Controller
    {
        // GET: API/Products
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult getProductsPerNetwork(int NetworkId, string Token)
        {
            //verifica del token


            //verifica passata
            ProductAPI oRet = new ProductAPI();
            ProductBLL oProductBLL = new Models.BLL.ProductBLL();

            oRet = oProductBLL.getProductsPerNetwork_test(NetworkId);

            return Json(oRet, JsonRequestBehavior.AllowGet);
        }
    }
}