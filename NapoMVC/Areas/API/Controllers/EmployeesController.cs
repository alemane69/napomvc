﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClassRepository;
using NapoMVC.Models.BLL;
namespace NapoMVC.Areas.API.Controllers
{
    public class EmployeesController : Controller
    {
        // GET: API/Employee
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult getEmployeesPerNetwork(int NetworkId, string Token)
        {
            //verifica del token


            //verifica passata
            EmployeeAPI oRet = new EmployeeAPI();
            EmployeeBLL oEmployeeBLL = new Models.BLL.EmployeeBLL();
            oRet = oEmployeeBLL.getEmployeesInNetworkAPI(NetworkId);
            return Json(oRet, JsonRequestBehavior.AllowGet);
        }
    }
}