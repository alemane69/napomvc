﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClassRepository;
using NapoMVC.Models.BLL;

namespace NapoMVC.Areas.API.Controllers
{
    public class ConnectionTestController : Controller
    {
        // GET: API/ConnectionTest
        public ActionResult Index()
        {
            ConnectionBLL oConnBLL = new Models.BLL.ConnectionBLL();
            ResponseStatus _status = oConnBLL.testConnection();
            return Json(_status, JsonRequestBehavior.AllowGet);
        }
    }
}