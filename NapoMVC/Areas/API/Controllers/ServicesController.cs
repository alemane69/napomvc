﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClassRepository;
using NapoMVC.Models.BLL;
namespace NapoMVC.Areas.API.Controllers
{
    public class ServicesController : Controller
    {
        // GET: API/Service
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult getServicesPerNetwork(int NetworkId, string Token)
        {
            //verifica del token


            //verifica del network -> dal token sono risalito al network id
            //UserBLL oUserBLL = new UserBLL();
            //int _networkId = oUserBLL.getUserNetwork(User.Identity.Name);
            //if (_networkId == NetworkId)
            //{
            //    ResponseStatus _status = new ResponseStatus();
            //    _serviceType = oServiceBLL.setService(_serviceType, out _status);
            //    oRet.Status = _status;
            //    oRet.Type = _serviceType;
            //}
            //else
            //{
            //    oRet.Status.Status = -100;
            //    oRet.Status.Error = "Errore id Network";
            //}

            //verifica passata
            ServiceAPI oRet = new ServiceAPI();
            ServiceBLL oServiceBLL = new Models.BLL.ServiceBLL();

            oRet.Services = oServiceBLL.getServicesInNetwork(NetworkId); ;
            //oRet = oServiceBLL.getServicesPerNetwork_test(NetworkId);
            return Json(oRet, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getServicesTypesPerNetwork(int NetworkId, string Token)
        {
            //verifica del token


            //verifica passata
            ServiceTypeAPI oRet = new ServiceTypeAPI();
            ServiceBLL oServiceBLL = new Models.BLL.ServiceBLL();
            oRet = oServiceBLL.getServiceTypesPerNetwork(NetworkId);
            return Json(oRet, JsonRequestBehavior.AllowGet);
        }
    }
}