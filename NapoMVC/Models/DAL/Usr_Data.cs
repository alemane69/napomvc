//------------------------------------------------------------------------------
// <auto-generated>
//     Codice generato da un modello.
//
//     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
//     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NapoMVC.Models.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Usr_Data
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int NetworkId { get; set; }
    }
}
