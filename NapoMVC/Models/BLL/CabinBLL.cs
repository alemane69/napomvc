﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NapoMVC.Models.DAL;
using ClassRepository;

namespace NapoMVC.Models.BLL
{
    public class CabinBLL
    {
        public List<Cabin> getCabinsInNetwork(int NetworkId)
        {
            List<Cabin> oRet = new List<Cabin>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Cabins where tab.NetworkId == NetworkId select tab;
                if (q.Count() > 0)
                {
                    foreach (var el in q.ToList())
                    {
                        Cabin _tmp = new Cabin();
                        _tmp.ID = el.ID;
                        _tmp.Code = el.Code;
                        _tmp.Name = el.Name;
                        _tmp.NetworkId = el.NetworkId;
                        oRet.Add(_tmp);
                        db.SaveChanges();
                    }
                }
            }
            return oRet;
        }
        public ResponseStatus CabinUpdate(Cabin oUp)
        {
            ResponseStatus oRet = new ResponseStatus();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Cabins where tab.ID == oUp.ID select tab;
                if (q.Count() > 0)
                {
                    Reg_Cabins _tmp = q.First();
                    _tmp.Code = oUp.Code;
                    _tmp.Name = oUp.Name;
                    _tmp.NetworkId = oUp.NetworkId;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch(Exception ex)
                    {
                        oRet.Status = -1;
                        oRet.Error = ex.Message;
                    }
                }
            }
            return oRet;

        }
    }
}