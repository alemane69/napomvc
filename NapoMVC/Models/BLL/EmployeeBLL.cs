﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NapoMVC.Models.DAL;
using ClassRepository;
namespace NapoMVC.Models.BLL
{
    public class EmployeeBLL
    {
        public List<Employee> getEmployeesInNetwork(int NetworkId)
        {
            List<Employee> oRet = new List<Employee>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Employees where tab.NetworkId == NetworkId select tab;
                if(q.Count()>0)
                {
                    foreach(var el in q.ToList())
                    {
                        Employee _tmp = new Employee();
                        _tmp.ID = el.ID;
                        _tmp.LastName = el.LastName;
                        _tmp.Name = el.Name;
                        _tmp.NetworkId = el.NetworkId;
                        _tmp.BirthNation = el.BirthNation;
                        _tmp.BirthPlace = el.BirthPlace;
                        _tmp.CityAdress = el.CityAddress;
                        _tmp.Email = el.Email;
                        _tmp.GUID = el.GUID;
                        _tmp.Obsolete = el.Obsolete;
                        _tmp.ResidenceAdress = el.ResidenceAddress;
                        _tmp.SaleList = el.SaleListId;
                        _tmp.Telephone = el.Telephone;
                        _tmp.UserId = el.UserId;
                        _tmp.ZipAdress = el.ZipAddress;
                        _tmp.Code = el.Codice;
                        oRet.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
        public EmployeeAPI getEmployeesInNetworkAPI(int NetworkId)
        {
            EmployeeAPI oRet = new EmployeeAPI();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Employees where tab.NetworkId == NetworkId select tab;
                if (q.Count() > 0)
                {
                    foreach (var el in q.ToList())
                    {
                        Employee _tmp = new Employee();
                        _tmp.ID = el.ID;
                        _tmp.LastName = el.LastName;
                        _tmp.Name = el.Name;
                        _tmp.NetworkId = el.NetworkId;
                        _tmp.BirthNation = el.BirthNation;
                        _tmp.BirthPlace = el.BirthPlace;
                        _tmp.CityAdress = el.CityAddress;
                        _tmp.Email = el.Email;
                        _tmp.GUID = el.GUID;
                        _tmp.Obsolete = el.Obsolete;
                        _tmp.ResidenceAdress = el.ResidenceAddress;
                        _tmp.SaleList = el.SaleListId;
                        _tmp.Telephone = el.Telephone;
                        _tmp.UserId = el.UserId;
                        _tmp.ZipAdress = el.ZipAddress;
                        _tmp.Code = el.Codice;
                        oRet.Employees.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
        public ResponseStatus EmployeeUpdate(Employee oUp)
        {
            ResponseStatus oRet = new ResponseStatus();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Employees where tab.ID == oUp.ID select tab;
                if (q.Count() > 0)
                {
                    Reg_Employees _tmp = q.First();
                    _tmp.LastName = oUp.LastName;
                    _tmp.Name = oUp.Name;
                    _tmp.NetworkId = oUp.NetworkId;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        oRet.Status = -1;
                        oRet.Error = ex.Message;
                    }
                }
            }
            return oRet;

        }



        public EmployeeAPI getEmployeesPerNetwork_test(int NetworkId)
        {
            EmployeeAPI oRet = new EmployeeAPI();
            Employee _cus1 = new Employee();
            _cus1.GUID = "xxeerrjshdhdhd";
            _cus1.ID = 1;
            _cus1.LastName = "Manetti";
            _cus1.Name = "Alessandro";
            _cus1.NetworkId = NetworkId;
            oRet.Employees.Add(_cus1);
            Employee _cus2 = new Employee();
            _cus2.GUID = "xxeerrjoiojdhdhd";
            _cus2.ID = 15;
            _cus2.LastName = "Somovigo";
            _cus2.Name = "Andrea";
            _cus2.NetworkId = NetworkId;
            oRet.Employees.Add(_cus2);
            Employee _cus3 = new Employee();
            _cus3.GUID = "xxeerrjoiojdhdhd";
            _cus3.ID = 85;
            _cus3.LastName = "Stefania";
            _cus3.Name = "Giannese";
            _cus3.NetworkId = NetworkId;
            oRet.Employees.Add(_cus3);
            return oRet;
        }


        public EmployeeAPI setEmployee(Employee oUpIns)
        {
            EmployeeAPI oRet = new EmployeeAPI();
            using(var db = new NapoEntities())
            {
                var _ins = false;
                Reg_Employees _tmp = null;
                var qTest = from tab in db.Reg_Employees where tab.ID == oUpIns.ID select tab;
                if(qTest.Count()==0)
                {
                    _ins = true;
                    _tmp = new DAL.Reg_Employees();
                }
                else
                {
                    _tmp = qTest.First();
                }
                _tmp.BirthNation = oUpIns.BirthNation;
                _tmp.BirthPlace = oUpIns.BirthPlace;
                _tmp.CityAddress = oUpIns.CityAdress;
                _tmp.Codice = oUpIns.Code;
                _tmp.Email = oUpIns.Email;
                _tmp.GUID = oUpIns.GUID;
                if (_tmp.GUID + "" == "")
                {
                    _tmp.GUID = GuidManagerBLL.RandomString(16, "Reg_Employees", oUpIns.NetworkId);
                }
                _tmp.LastName = oUpIns.LastName;
                _tmp.Name = oUpIns.Name;
                _tmp.NetworkId = oUpIns.NetworkId;
                _tmp.Obsolete = oUpIns.Obsolete;
                _tmp.ResidenceAddress = oUpIns.ResidenceAdress;
                _tmp.SaleListId = oUpIns.SaleList;
                _tmp.Telephone = oUpIns.Telephone;
                _tmp.UserId = oUpIns.UserId;
                _tmp.ZipAddress = oUpIns.ZipAdress;
                if (_ins)
                    db.Reg_Employees.Add(_tmp);
                try
                {
                    db.SaveChanges();
                }
                catch(Exception ex)
                {
                    oRet.Status.Error = ex.Message;
                    oRet.Status.Status = -200;
                }
                oUpIns.ID = _tmp.ID;
                oRet.Employees.Add(oUpIns);

            }
            return oRet;
        }
    }
}