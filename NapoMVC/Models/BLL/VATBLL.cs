﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NapoMVC.Models.DAL;
using ClassRepository;
using System.Data.SqlClient;
using System.Configuration;

namespace NapoMVC.Models.BLL
{
    public class VATBLL
    {
        public VATBLL()
        {

        }
        public List<VAT> getVatPerNetwork(int NetworkId)
        {
            List<VAT> oRet = new List<VAT>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_VAT where tab.NetworkId == NetworkId select tab;
                if(q.Count()>0)
                {
                    foreach(var el in q.ToList())
                    {
                        VAT _tmp = new VAT();
                        _tmp.Code = el.Code;
                        _tmp.DescriptionInRow = el.DescriptionInRow;
                        _tmp.GUID = el.GUID;
                        _tmp.Id = el.ID;
                        _tmp.Name = el.Name;
                        _tmp.NetworkId = el.NetworkId;
                        _tmp.Value = el.Value;
                        oRet.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
    }
}