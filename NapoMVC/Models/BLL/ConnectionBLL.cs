﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClassRepository;
using NapoMVC.Models.DAL;
namespace NapoMVC.Models.BLL
{
    public class ConnectionBLL
    {

        public ResponseStatus testConnection()
        {
            ResponseStatus oRet = new ResponseStatus();
            try
            {
                using (var db = new NapoEntities())
                {
                    var q = from tab in db.Reg_VAT select tab;
                    if (q.Count() > 0)
                    {
                        oRet.Error = "";
                        oRet.Status = 0;
                    }
                }
            }
            catch
            {
                oRet.Status = -1;
                oRet.Error = "DB connection is down";
            }
            return oRet;
        }
    }
}