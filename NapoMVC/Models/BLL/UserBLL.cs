﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClassRepository;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using NapoMVC.Models.DAL;
namespace NapoMVC.Models.BLL
{
    public class UserBLL
    {
        public List<Network> getNetworks()
        {
            List<Network> oRet = new List<Network>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Networks orderby tab.Name ascending select tab;
                if(q.Count()>0)
                {
                    foreach(var el in q.ToList())
                    {
                        Network _tmp = new Network();
                        _tmp.Id = el.Id;
                        _tmp.Name = el.Name;
                        _tmp.PreName = el.PreCode;
                        oRet.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
        public int getUserNetwork(string UserName)
        {
            int ret = 0;
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Usr_Data where tab.UserName == UserName select tab;
                if (q.Count() >0)
                    ret = q.First().NetworkId;
            }
            return ret;
        }
        public List<UserNetwork> getUserNetworkNonEmployee(int NetworkId, int UserId=0)
        {
            List<UserNetwork> oRet = new List<UserNetwork>();
            string _conn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlCommand cmLoc = null;
            SqlConnection cnLoc = null;
            try
            {
                cnLoc = new SqlConnection(_conn);
                cmLoc = new SqlCommand();
                cmLoc.Connection = cnLoc;
                cmLoc.CommandType = CommandType.Text;
                cmLoc.CommandText = "select usr.* from AspNetUsers as usr join Usr_Data as ud on usr.Id = ud.UserId where (usr.Code not in(select UserId from Reg_Employees where NetworkId=" + NetworkId + ") or usr.Code = " + UserId + ") and ud.NetworkId=" + NetworkId;
                cnLoc.Open();
                SqlDataReader rsLoc = cmLoc.ExecuteReader();
                while(rsLoc.Read())
                {
                    UserNetwork _tmp = new UserNetwork();
                    _tmp.Code = Convert.ToInt32(rsLoc["Code"]);
                    _tmp.Id = Convert.ToString(rsLoc["Id"]);
                    _tmp.NetwotkId = NetworkId;
                    _tmp.UserName = Convert.ToString(rsLoc["UserName"]);
                    oRet.Add(_tmp);
                }
            }
            catch(Exception ex)
            {
                cnLoc.Close();
                cnLoc.Dispose();
                cmLoc.Dispose();
            }
            return oRet;
        }
    }
}