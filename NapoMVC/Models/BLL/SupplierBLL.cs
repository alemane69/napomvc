﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NapoMVC.Models.DAL;
using ClassRepository;

namespace NapoMVC.Models.BLL
{
    public class SupplierBLL
    {
        public List<Supplier> getSuppliersInNetwork(int NetworkId)
        {
            List<Supplier> oRet = new List<Supplier>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Suppliers where tab.NetworkId == NetworkId select tab;
                if (q.Count() > 0)
                {
                    foreach (var el in q.ToList())
                    {
                        Supplier _tmp = new Supplier();
                        _tmp.ID = el.ID;
                        _tmp.Code = el.Code;
                        _tmp.Name = el.Name;
                        _tmp.NetworkId = el.NetworkId;
                        oRet.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
        public ResponseStatus SupplierUpdate(Supplier oUp)
        {
            ResponseStatus oRet = new ResponseStatus();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Suppliers where tab.ID == oUp.ID select tab;
                if (q.Count() > 0)
                {
                    Reg_Suppliers _tmp = q.First();
                    _tmp.Code = oUp.Code;
                    _tmp.Name = oUp.Name;
                    _tmp.NetworkId = oUp.NetworkId;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        oRet.Status = -1;
                        oRet.Error = ex.Message;
                    }
                }
            }
            return oRet;

        }
    }
}