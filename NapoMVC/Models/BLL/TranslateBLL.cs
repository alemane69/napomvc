﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClassRepository;
using System.Data;
using System.Data.SqlClient;
using NapoMVC.Models.DAL;
namespace NapoMVC.Models.BLL
{
    public class TranslateBLL
    {
        private string _lang;
        private string _context;
        private int _langId;
        private List<LabelTranslation> _lst;
        public TranslateBLL(string Lang, string Context)
        {
            this._lang = Lang;
            this._context = Context;
            this._lst = new List<LabelTranslation>();
            this._langId = this.getLangFromStringCode(Lang).Id;
            using (var db = new NapoEntities())
            {
                var q = (from tab in db.Lng_Translation where tab.Context == this._context && tab.Language == this._lang select tab);
                if (q.Count() > 0)
                {
                    foreach (var el in q.ToList())
                    {
                        LabelTranslation _tmp = new LabelTranslation();
                        _tmp.Context = this._context;
                        _tmp.Key = el.LabelKey;
                        _tmp.Language = this._lang;
                        _tmp.Translation = el.LabelTranslation;
                        _tmp.TranslationExtension = el.LabelTranslationExtension;
                        this._lst.Add(_tmp);
                    }
                }
            };
        }
        public LabelTranslation getTranslation(string Key)
        {
            LabelTranslation oRet = new LabelTranslation();
            oRet.Context = this._context;
            oRet.Key = Key;
            oRet.Language = this._lang;
            oRet.Translation = Key;
            oRet.TranslationExtension = Key;
            var qLst = (from x in this._lst where x.Key.ToLower() == Key.ToLower() select x);
            if (qLst.Count() > 0)
            {
                oRet.Translation = qLst.First().Translation;
                oRet.TranslationExtension = qLst.First().TranslationExtension;
            }
            else
            {
                using (var db = new NapoEntities())
                {
                    var q = (from tab in db.Lng_Translation where tab.Context == "" && tab.Language == this._lang && tab.LabelKey == Key select tab);
                    if (q.Count() > 0)
                    {
                        oRet.Translation = q.First().LabelTranslation;
                        oRet.TranslationExtension = q.First().LabelTranslationExtension;
                    }
                    else
                    {
                        //inserisco avercelo da aggiornare
                        foreach (var el in this.getLang())
                        {
                            var qTest = from tab in db.Lng_Translation where tab.Context == this._context && tab.Language == el.Code && tab.LabelKey == Key select tab;
                            if (qTest.Count() == 0)
                            {
                                Lng_Translation _oIns = new Lng_Translation();
                                _oIns.Context = this._context;
                                _oIns.LabelKey = Key;
                                _oIns.LabelTranslation = Key;
                                _oIns.LabelTranslationExtension = Key;
                                _oIns.Language = el.Code;
                                _oIns.LanguageId = el.Id;
                                db.Lng_Translation.Add(_oIns);
                                db.SaveChanges();
                            }
                        }

                    }
                }
            }
            return oRet;
        }
        public List<Lang> getLang()
        {
            List<Lang> oRet = new List<Lang>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Languages where tab.IsActive == true select tab;
                if (q.Count() > 0)
                {
                    foreach (var el in q.ToList())
                    {
                        Lang _tmp = new Lang();
                        _tmp.Code = el.Code;
                        _tmp.Id = el.ID;
                        _tmp.Name = el.Name;
                        oRet.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
        public Lang getLangFromStringCode(string Lang)
        {
            Lang oRet = new Lang();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Languages where tab.Code == Lang select tab;
                if (q.Count() > 0)
                {
                    oRet.Code = q.First().Code;
                    oRet.Id = q.First().ID;
                    oRet.Name = q.First().Name;
                }
            }
            return oRet;
        }
    }
}