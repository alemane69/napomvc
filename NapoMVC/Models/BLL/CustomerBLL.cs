﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NapoMVC.Models.DAL;
using ClassRepository;

namespace NapoMVC.Models.BLL
{
    public class CustomerBLL
    {
        public List<Customer> getCustomersInNetwork(int NetworkId)
        {
            List<Customer> oRet = new List<Customer>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Customers where tab.NetworkId == NetworkId select tab;
                if (q.Count() > 0)
                {
                    foreach (var el in q.ToList())
                    {
                        Customer _tmp = new Customer();
                        _tmp.ID = el.ID;
                        _tmp.LastName = el.LastName;
                        _tmp.Name = el.Name;
                        _tmp.NetworkId = el.NetworkId;
                        oRet.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
        public ResponseStatus CustomerUpdate(Customer oUp)
        {
            ResponseStatus oRet = new ResponseStatus();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Customers where tab.ID == oUp.ID select tab;
                if (q.Count() > 0)
                {
                    Reg_Customers _tmp = q.First();
                    _tmp.LastName = oUp.LastName;
                    _tmp.Name = oUp.Name;
                    _tmp.NetworkId = oUp.NetworkId;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        oRet.Status = -1;
                        oRet.Error = ex.Message;
                    }
                }
            }
            return oRet;

        }



        public CustomerAPI getCustomerPerNetwork_test(int NetworkId)
        {
            CustomerAPI oRet = new CustomerAPI();
            Customer _cus1 = new Customer();
            _cus1.GUID = "xxeerrjshdhdhd";
            _cus1.ID = 1;
            _cus1.LastName = "Manetti";
            _cus1.Name = "Alessandro";
            _cus1.NetworkId = NetworkId;
            _cus1.Email = "ale@ale.it";
            _cus1.Telephone = "00000";
            oRet.Customers.Add(_cus1);
            Customer _cus2 = new Customer();
            _cus2.GUID = "xxeerrjoiojdhdhd";
            _cus2.ID = 15;
            _cus2.LastName = "Somovigo";
            _cus2.Name = "Andrea";
            _cus2.NetworkId = NetworkId;
            _cus2.Telephone = "111111";
            _cus2.Email = "andrea@andrea.it";
            oRet.Customers.Add(_cus2);
            Customer _cus3 = new Customer();
            _cus3.GUID = "xxeerrjoiokjygdhd";
            _cus3.ID = 85;
            _cus3.LastName = "Stefania";
            _cus3.Name = "Giannese";
            _cus3.NetworkId = NetworkId;
            _cus3.Email = "ste@ste.it";
            _cus3.Telephone = "33333";
            oRet.Customers.Add(_cus3);
            return oRet;
        }
    }
}