﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
namespace NapoMVC.Models.BLL
{
    public static class GuidManagerBLL
    {

        private static Random random = new Random();
        public static string RandomString(int length,string TableName,int NetworkId)
        {
            string ret = "";
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            ret = new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
            while(!isFreeGUID(ret,TableName,NetworkId))
            {
                ret = RandomString(16, TableName,NetworkId);
            }
            return ret;
        }
        private static bool isFreeGUID(string GUID, string TableName,int NetworkId)
        {
            bool ret = true;
            string _conn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlCommand cmLoc = null;
            SqlConnection cnLoc = null;
            try
            {
                cnLoc = new SqlConnection(_conn);
                cmLoc = new SqlCommand();
                cmLoc.CommandType = System.Data.CommandType.Text;
                cmLoc.Connection = cnLoc;
                cmLoc.CommandText = "Select Count(*) as Num from " + TableName + " where GUID ='" + GUID + "' and NetoworkId=" + NetworkId;
                cnLoc.Open();
                SqlDataReader rsLoc = cmLoc.ExecuteReader();
                if(rsLoc.Read())
                {
                    int _num = Convert.ToInt32(rsLoc["Num"]);
                    if (_num > 0)
                        ret = false;
                }
            }
            catch(Exception ex)
            {

            }
            finally
            {
                cnLoc.Close();
                cnLoc.Dispose();
                cmLoc.Dispose();
            }
            return ret;
        }
    }
}