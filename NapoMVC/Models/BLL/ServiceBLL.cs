﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NapoMVC.Models.DAL;
using ClassRepository;
using System.Data.SqlClient;
using System.Configuration;
namespace NapoMVC.Models.BLL
{
    public class ServiceBLL
    {
        #region gestione Servizi
        public List<Service> getServicesInNetwork(int NetworkId)
        {
            List<Service> oRet = new List<Service>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Services join tab1 in db.Reg_VAT on tab.VATId equals tab1.ID where tab.NetworkId == NetworkId select new { tab = tab, vatValue=tab1.Value};
                if (q.Count() > 0)
                {
                    foreach (var el in q.ToList())
                    {
                        Service _tmp = new Service();
                        _tmp.ID = el.tab.ID;
                        _tmp.GUID = el.tab.GUID;
                        _tmp.Code = el.tab.Code;
                        _tmp.Name = el.tab.Name;
                        _tmp.NetworkId = el.tab.NetworkId;
                        //_tmp.ServiceType1 = this.getServiceTypeById(el.tab.Type1,NetworkId);
                        //_tmp.ServiceType2 = this.getServiceTypeById(el.tab.Type2,NetworkId);
                        _tmp.ServiceType = new ServiceTypeComplex();
                        _tmp.ServiceType.ServiceTypeParent = this.getServiceTypeById(el.tab.Type1, NetworkId); 
                        _tmp.ServiceType.ServiceTypeChild = this.getServiceTypeById(el.tab.Type2, NetworkId);
                        _tmp.Price = el.tab.Price;
                        _tmp.WorkingMinutes = el.tab.WorkingTime;
                        _tmp.Minutes = el.tab.CustomerTime;
                        _tmp.VAT = el.tab.VATId;
                        _tmp.ValueVAT = el.vatValue;
                        var qList = from tab in db.Srv_SaleListServices where tab.ServiceId == el.tab.ID select tab;
                        if (qList.Count() > 0)
                        {
                            foreach (var lst in qList.ToList())
                            {
                                ServicePrices _price = new ServicePrices();
                                _price.Price = lst.Price;
                                _price.SaleListId = lst.SaleList;
                                _tmp.SaleListPrices.Add(_price);
                            }
                        }
                        oRet.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
        public List<ServiceGrid> getServicesGridInNetwork(int NetworkId)
        {
            List<ServiceGrid> oRet = new List<ServiceGrid>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Services join tab1 in db.Reg_VAT on tab.VATId equals tab1.ID where tab.NetworkId == NetworkId select new { tab = tab, vatValue = tab1.Value };
                if (q.Count() > 0)
                {
                    foreach (var el in q.ToList())
                    {
                        ServiceGrid _tmp = new ServiceGrid();
                        _tmp.ID = el.tab.ID;
                        _tmp.GUID = el.tab.GUID;
                        _tmp.Code = el.tab.Code;
                        _tmp.Name = el.tab.Name;
                        _tmp.NetworkId = el.tab.NetworkId;
                        _tmp.ServiceTypeId1 = el.tab.Type1;
                        _tmp.ServiceTypeId2 = el.tab.Type2;
                        _tmp.ServiceTypeName1 = this.getServiceTypeById(el.tab.Type1, NetworkId).Name;
                        _tmp.ServiceTypeName2 = this.getServiceTypeById(el.tab.Type2, NetworkId).Name;
                        _tmp.Price = el.tab.Price;
                        _tmp.WorkingMinutes = el.tab.WorkingTime;
                        _tmp.VAT = el.vatValue;
                        _tmp.VATId = el.tab.VATId;
                        _tmp.Minutes = el.tab.CustomerTime;
                        var qList = from tab in db.Srv_SaleListServices where tab.ServiceId == el.tab.ID select tab;
                        if(qList.Count()>0)
                        {
                            foreach(var lst in qList.ToList())
                            {
                                ServicePrices _price = new ServicePrices();
                                _price.Price = lst.Price;
                                _price.SaleListId = lst.SaleList;
                                _tmp.SaleListPrices.Add(_price);
                            }
                        }
                        oRet.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
        public ResponseStatus ServiceUpdate(Service oUp)
        {
            ResponseStatus oRet = new ResponseStatus();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Services where tab.ID == oUp.ID select tab;
                if (q.Count() > 0)
                {
                    Reg_Services _tmp = q.First();
                    _tmp.Code = oUp.Code;
                    _tmp.Name = oUp.Name;
                    _tmp.NetworkId = oUp.NetworkId;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        oRet.Status = -1;
                        oRet.Error = ex.Message;
                    }
                }
            }
            return oRet;

        }
        public Service setService(Service oService,out ResponseStatus Status)
        {
            Status = new ResponseStatus();
            using (var db = new NapoEntities())
            {
                if(oService.Name=="")
                {
                    Status.Status = -200;
                    Status.Error = "Il campo nome deve essere diverso da vuoto";
                }
                bool _ins = false;
                Reg_Services _tmp = null;
                var q = from tab in db.Reg_Services where tab.ID == oService.ID select tab;
                if(q.Count()>0)
                {
                    _tmp = q.First();
                }
                else
                {
                    _tmp = new Reg_Services();
                    _ins = true;
                }
                _tmp.Code = oService.Code;
                _tmp.GUID = oService.GUID;
                if(_tmp.GUID + "" == "")
                {
                    _tmp.GUID = GuidManagerBLL.RandomString(16, "Reg_Services",oService.NetworkId);
                }
                _tmp.Name = oService.Name;
                _tmp.NetworkId = oService.NetworkId;
                _tmp.Price = oService.Price;
                _tmp.Type1 = oService.ServiceType.ServiceTypeParent.Id;
                _tmp.Type2 = oService.ServiceType.ServiceTypeChild.Id;
                _tmp.WorkingTime = oService.WorkingMinutes;
                _tmp.CustomerTime = oService.Minutes;
                _tmp.VATId = oService.VAT;
                _tmp.Code = oService.Code;
                if (_ins)
                    db.Reg_Services.Add(_tmp);
                try
                {
                    db.SaveChanges();
                    oService.ID = _tmp.ID;
                    oService.GUID = _tmp.GUID;
                }
                catch(Exception ex)
                {
                    Status.Status = -1;
                    Status.Error = ex.Message;
                }
                oService.ID = _tmp.ID;
                oService.GUID = _tmp.GUID;
                //aggiorno i listini
                foreach(var lst in oService.SaleListPrices)
                {
                    if(!this.setSalistPrice(lst.SaleListId,oService.ID,lst.Price))
                    {
                        Status.Error = "Error in salelist " + lst.SaleListId + " for the service " + oService.ID;
                        Status.Status = -200;
                    }
                }
            }
            return oService;
        }
        public Service getServiceById(long ServiceId)
        {
            Service oRet = new Service();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Services join tab1 in db.Reg_VAT on tab.VATId equals tab1.Value where tab.ID == ServiceId select new { tab = tab, vatValue=tab1.Value,vatId = tab1.ID };
                if(q.Count()>0)
                {
                    oRet.GUID = q.First().tab.GUID;
                    oRet.ID = q.First().tab.ID;
                    oRet.Minutes = q.First().tab.CustomerTime;
                    oRet.Name = q.First().tab.Name;
                    oRet.NetworkId = q.First().tab.NetworkId;
                    oRet.WorkingMinutes = q.First().tab.WorkingTime;
                    oRet.VAT = q.First().vatId;
                    oRet.ValueVAT = q.First().vatValue;
                    oRet.Code = q.First().tab.Code;
                    oRet.Price = q.First().tab.Price;
                    oRet.ServiceType.ServiceTypeParent = this.getServiceTypeById(q.First().tab.Type1,q.First().tab.NetworkId);
                    oRet.ServiceType.ServiceTypeChild = this.getServiceTypeById(q.First().tab.Type2, q.First().tab.NetworkId);
                }
            }
            return oRet;
        }
        public int getServiceFullDuration(long ServiceId)
        {
            string _conn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            int ret = 0;
            SqlCommand cmLoc = null;
            SqlConnection cnLoc = null;
            try
            {
                cnLoc = new SqlConnection(_conn);
                cmLoc = new SqlCommand();
                cmLoc.CommandType = System.Data.CommandType.Text;
                cmLoc.Connection = cnLoc;
                cmLoc.CommandText = "Select coalesce(sum(duration) ,0)as Duration from Reg_ServiceDurations where ServiceId=" + ServiceId;
                cnLoc.Open();
                SqlDataReader rsLoc = cmLoc.ExecuteReader();
                if (rsLoc.Read())
                {
                    ret = Convert.ToInt32(rsLoc["Duration"]);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cnLoc.Close();
                cnLoc.Dispose();
                cmLoc.Dispose();
            }
            return ret;
        }
        public bool setSalistPrice(int SaleListId,long ServiceId,decimal SaleListPrice)
        {
            bool ret = true;
            using (var db = new NapoEntities())
            {
                bool _ins = true;
                Srv_SaleListServices _tmp = null;
                var q = from tab in db.Srv_SaleListServices where tab.SaleList == SaleListId && tab.ServiceId == ServiceId select tab;
                if(q.Count()>0)
                {
                    _ins = false;
                    _tmp = q.First();
                }
                else
                {
                    _tmp = new Srv_SaleListServices();
                    _tmp.SaleList = SaleListId;
                    _tmp.ServiceId = ServiceId;
                    _tmp.Price = SaleListPrice;
                }
                _tmp.Price = SaleListPrice;
                if(_ins)
                {
                    db.Srv_SaleListServices.Add(_tmp);
                }
                try
                {
                    db.SaveChanges();
                }
                catch(Exception ex)
                {
                    ret = false;
                }
            }
            return ret;
        }
        #endregion


        #region gestione tipi servizio
        public ServiceTypeAPI getServiceTypesPerNetwork(int NetworkId)
        {
            ServiceTypeAPI oRet = new ServiceTypeAPI();
            oRet.Status = new ResponseStatus();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_ServiceTypes where tab.NetworkId == NetworkId select tab;
                if(q.Count()>0)
                {
                    foreach(var el in q.ToList().Where(x=>x.ParentId == x.ID))
                    {
                        
                        TypeComplete _type = new TypeComplete();
                        _type.ServiceType = new ServiceTypesBase();
                        _type.ServiceType.Id = el.ID;
                        _type.ServiceType.Name = el.Name;
                        _type.ServiceType.GUID = el.GUID;
                        _type.ServiceType.ParentId = _type.ServiceType.Id;
                        _type.ServiceType.NetworkId = NetworkId;
                        var qC = from x in q.ToList() where x.ParentId == el.ID && x.ID != el.ID select x;
                        if(qC.Count()>0)
                        {
                            _type.ServiceTypeChildren = new List<ServiceTypesBase>();
                            foreach(var c in qC.ToList())
                            {
                                ServiceTypesBase _tmpB = new ServiceTypesBase();
                                _tmpB.Id = c.ID;
                                _tmpB.Name = c.Name;
                                _tmpB.GUID = c.GUID;
                                _type.ServiceTypeChildren.Add(_tmpB);
                                _type.ServiceType.ParentId = el.ID;
                                _tmpB.NetworkId = NetworkId;
                            }
                        }
                        oRet.ServiceTypes.Add(_type);
                    }
                }
            }
            return oRet;
        }
        public List<ServiceTypesBase> getServiceTypesPerNetworkGrid(int NetworkId)
        {
            List<ServiceTypesBase> oRet = new List<ServiceTypesBase>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_ServiceTypes where tab.NetworkId == NetworkId select tab;
                if(q.Count()>0)
                {
                    foreach(var el in q.ToList())
                    {
                        ServiceTypesBase _tmp = new ServiceTypesBase();
                        _tmp.GUID = el.GUID;
                        _tmp.Id = el.ID;
                        _tmp.Name = el.Name;
                        _tmp.ParentId = el.ParentId;
                        _tmp.NetworkId = NetworkId;
                        if (el.ParentId == el.ID)
                            _tmp.ParentName = "";
                        else
                            _tmp.ParentName = this.getServiceTypeById(el.ParentId, NetworkId).Name;
                        oRet.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
        
        public ServiceTypesBase getServiceTypeById(int ServiceTypeId,int NetworkId)
        {
            ServiceTypesBase oRet = new ServiceTypesBase();
            oRet.NetworkId = NetworkId;
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_ServiceTypes where tab.ID == ServiceTypeId && tab.NetworkId==NetworkId select tab;
                if (q.Count() > 0)
                {
                    oRet.GUID = q.First().GUID;
                    oRet.Id = q.First().ID;
                    oRet.Name = q.First().Name;
                    oRet.ParentId = q.First().ParentId;
                    oRet.NetworkId = q.First().NetworkId;
                    if (oRet.NetworkId != NetworkId)
                        return new ServiceTypesBase();
                }
            }
            return oRet;
        }
        public ServiceTypesBase setServiceType(ServiceTypesBase oUp, out ResponseStatus Status)
        {
            ServiceTypesBase oRet = new ServiceTypesBase();
            Status = new ResponseStatus();
            bool _ins = true;
            using (var db = new NapoEntities())
            {
                Reg_ServiceTypes _tmp = null;
                var qTest = from tab in db.Reg_ServiceTypes where tab.ID == oUp.Id select tab;
                if (qTest.Count() > 0)
                {
                    _ins = false;
                    _tmp = qTest.First();
                }
                else
                    _tmp = new Reg_ServiceTypes();
                if (oUp.GUID + "" == "")
                    oUp.GUID = GuidManagerBLL.RandomString(16, "Reg_ServiceTypes",oUp.NetworkId);
                _tmp.GUID = oUp.GUID;
                _tmp.Name = oUp.Name;
                _tmp.NetworkId = oUp.NetworkId;
                _tmp.ParentId = oUp.ParentId;
                if (_ins)
                    db.Reg_ServiceTypes.Add(_tmp);
                db.SaveChanges();
                oUp.Id = _tmp.ID;
                var qParent = from tab in db.Reg_ServiceTypes where tab.ID == oUp.ParentId select tab;
                if(qParent.Count()== 0)
                {
                    var qUp = from tab in db.Reg_ServiceTypes where tab.ID == oUp.Id select tab;
                    if(qUp.Count()>0)
                    {
                        qUp.First().ParentId = qUp.First().ID;
                        try
                        {
                            db.SaveChanges();
                        }
                        catch(Exception ex)
                        {
                            Status.Error = ex.Message;
                            Status.Status = -1;
                        }
                    }
                }
                oRet = oUp;
            }
            return oRet;
        }
        public List<SaleList> getSalesListsPerNetwork(int NetworkId)
        {
            List<SaleList> oRet = new List<SaleList>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Srv_SaleLists where tab.NetworkId==NetworkId select tab;
                if (q.Count() > 0)
                {
                    foreach(var el in q.ToList())
                    {
                        SaleList _tmp = new SaleList();
                        _tmp.Code = el.Code;
                        _tmp.GUID = el.GUID;
                        _tmp.Id = el.ID;
                        _tmp.Name = el.Name;
                        _tmp.Note = el.Note;
                        oRet.Add(_tmp);
                    }
                }
            }
            return oRet;
        }
        #endregion

        #region gestione parte test
        //public ServiceAPI getServicesPerNetwork_test(int NetworkId)
        //{
        //    ServiceAPI oRet = new ServiceAPI();
        //    Service _cus1 = new Service();
        //    _cus1.GUID = "xxepcejshdhdhd";
        //    _cus1.ID = 1;
        //    _cus1.Name = "Messa in piega";
        //    _cus1.NetworkId = NetworkId;
        //    _cus1.Minutes = 15;
        //    _cus1.ServiceType1.Id = 5;
        //    _cus1.ServiceType1.Name = "Pettinatura";
        //    oRet.Services.Add(_cus1);
        //    Service _cus2 = new Service();
        //    _cus2.GUID = "xxeerrjoiojdhdhd";
        //    _cus2.ID = 15;
        //    _cus2.Name = "Taglio con forbici";
        //    _cus2.NetworkId = NetworkId;
        //    _cus2.ServiceType1.Id = 7;
        //    _cus2.ServiceType1.Name = "Taglio";
        //    oRet.Services.Add(_cus2);
        //    Service _cus3 = new Service();
        //    _cus3.GUID = "xxeeruytiojdhdhd";
        //    _cus3.ID = 85;
        //    _cus3.Name = "Taglio con macchinetta";
        //    _cus3.ServiceType1.Id = 7;
        //    _cus3.ServiceType1.Name = "Taglio";
        //    _cus3.NetworkId = NetworkId;
        //    oRet.Services.Add(_cus3);
        //    return oRet;
        //}
        #endregion
    }
}