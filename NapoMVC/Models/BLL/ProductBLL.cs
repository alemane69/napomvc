﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NapoMVC.Models.DAL;
using ClassRepository;
namespace NapoMVC.Models.BLL
{
    public class ProductBLL
    {
        public List<Product> getProductsInNetwork(int NetworkId)
        {
            List<Product> oRet = new List<Product>();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Products where tab.NetworkId == NetworkId select tab;
                if (q.Count() > 0)
                {
                    foreach (var el in q.ToList())
                    {
                        Product _tmp = new Product();
                        _tmp.ID = el.ID;
                        _tmp.Code = el.Code;
                        _tmp.Name = el.Name;
                        _tmp.NetworkId = el.NetworkId;
                        oRet.Add(_tmp);
                        db.SaveChanges();
                    }
                }
            }
            return oRet;
        }
        public ResponseStatus ProductUpdate(Product oUp)
        {
            ResponseStatus oRet = new ResponseStatus();
            using (var db = new NapoEntities())
            {
                var q = from tab in db.Reg_Products where tab.ID == oUp.ID select tab;
                if (q.Count() > 0)
                {
                    Reg_Products _tmp = q.First();
                    _tmp.Code = oUp.Code;
                    _tmp.Name = oUp.Name;
                    _tmp.NetworkId = oUp.NetworkId;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        oRet.Status = -1;
                        oRet.Error = ex.Message;
                    }
                }
            }
            return oRet;

        }


        public ProductAPI getProductsPerNetwork_test(int NetworkId)
        {
            ProductAPI oRet = new ProductAPI();
            Product _cus1 = new Product();
            _cus1.GUID = "xxeerrjrereshdhdhd";
            _cus1.ID = 4;
            _cus1.Code = "SHPLR250";
            _cus1.Name = "Shampo Loreal Super 250 ml";
            _cus1.Price = 25;
            _cus1.NetworkId = NetworkId;
            oRet.Products.Add(_cus1);
            Product _cus2 = new Product();
            _cus2.GUID = "jfsndjhbijuggg";
            _cus1.Code = "ANTLR01";
            _cus2.ID = 48;
            _cus2.Name = "Anticaduta Bio Repair";
            double _p = 18.5;
            _cus2.Price = Convert.ToDecimal(_p);
            _cus2.NetworkId = NetworkId;
            oRet.Products.Add(_cus2);
            Product _cus3 = new Product();
            _cus3.GUID = "xxeerrjohnfhfdhdhd";
            _cus3.ID = 96;
            double _p1 = 13.5;
            _cus3.Price = Convert.ToDecimal(_p1);
            _cus3.Name = "Balsamo super";
            _cus3.NetworkId = NetworkId;
            oRet.Products.Add(_cus3);
            return oRet;
        }
    }
}