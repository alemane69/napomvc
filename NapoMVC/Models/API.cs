﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NapoMVC.Models;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;



using Microsoft.Owin.Security;


namespace NapoMVC.Models
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext(): base("DefaultConnection")
        {

        }
    }
    public class AuthRepository : IDisposable
    {
        private AuthContext _ctx;

        private UserManager<IdentityUser> _userManager;

        public AuthRepository()
        {
            _ctx = new AuthContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));
        }

        public async Task<IdentityResult> RegisterUser(UserModel userModel)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = userModel.UserName
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);

            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }
    }
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private ApplicationSignInManager _signInManager;
        public SimpleAuthorizationServerProvider()
        {

        }
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        //public ApplicationSignInManager SignInManager
        //{
        //    get
        //    {
        //        return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
        //    }
        //    private set
        //    {
        //        _signInManager = value;
        //    }
        //}
        public override Task TokenEndpointResponse(OAuthTokenEndpointResponseContext context)
        {
            var accessToken = context.AccessToken;
            string userName = context.TokenEndpointRequest.ResourceOwnerPasswordCredentialsGrant.UserName;
            return Task.FromResult<object>(null);
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            using (AuthRepository _repo = new AuthRepository())
            {
                IdentityUser user = await _repo.FindUser(context.UserName, context.Password);
                //var SignInStatus = await SignInManager.PasswordSignInAsync(context.UserName, context.Password,false, shouldLockout: true);

                if (user == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim("sub", context.UserName));
                //aggingere i ruoli veri
                identity.AddClaim(new Claim("role", "user"));

                context.Validated(identity);
                
                //switch (SignInStatus)
                //{
                //    case SignInStatus.Success:
                //        context.SetError("");
                //        var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                //        identity.AddClaim(new Claim("sub", context.UserName));
                //        identity.AddClaim(new Claim("role", "user"));
                //        context.Validated(identity);
                //        return;
                //    //return RedirectToLocal(returnUrl);
                //    case SignInStatus.LockedOut:
                //        context.SetError("User bloccato");
                //        return;
                //    case SignInStatus.RequiresVerification:
                //        context.SetError("Utente non verificato");
                //        return;
                //    case SignInStatus.Failure:
                //    default:
                //        context.SetError("Tentativo di accesso non valido");
                //        return;
                //}
            }
        }
    }
}