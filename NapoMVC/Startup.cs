﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NapoMVC.Startup))]
namespace NapoMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
