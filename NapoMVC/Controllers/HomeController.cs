﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NapoMVC.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View("../backOffice/index");
        }
        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View("../backOffice/index");
        }
        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View("../backOffice/index");
        }
        public ActionResult TestBO()
        {
            return View();
        }
        public ActionResult TestDash()
        {
            return View();
        }
        public ActionResult TestKendo()
        {
            return View();
        }
    }
}