﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NapoMVC.Controllers
{
    public class BackOfficeController : Controller
    {
        // GET: BackOffice
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult Store()
        {
            return View();
        }
        [Authorize]
        public ActionResult Settings()
        {
            return View();
        }
        [Authorize]
        public ActionResult Employees()
        {
            NapoMVC.Models.BLL.UserBLL oUserBLL = new NapoMVC.Models.BLL.UserBLL();
            int _networkId = oUserBLL.getUserNetwork(User.Identity.Name);
            Models.BLL.ServiceBLL oServiceBLL = new Models.BLL.ServiceBLL();
            ViewData["SaleLists"] = oServiceBLL.getSalesListsPerNetwork(_networkId);
            return View();
        }
        [Authorize]
        public ActionResult EmployeeDetails(int EmployeeId)
        {
            return View();
        }
        [Authorize]
        public ActionResult Products()
        {
            return View();
        }
        [Authorize]
        public ActionResult ProductDetails(long ProductId)
        {
            return View();
        }
        [Authorize]
        public ActionResult ServiceTypeDetails(int ServiceTypeId,int NetworkId)
        {
            Models.BLL.ServiceBLL oServiceBLL = new Models.BLL.ServiceBLL();
            NapoMVC.Models.BLL.UserBLL oUserBLL = new NapoMVC.Models.BLL.UserBLL();

            List<ClassRepository.ServiceTypesBase> _lstService = oServiceBLL.getServiceTypesPerNetworkGrid(NetworkId);
            ClassRepository.ServiceTypesBase _tmpLoc = new ClassRepository.ServiceTypesBase();
            _tmpLoc.GUID = "";
            _tmpLoc.Id = 0;
            _tmpLoc.Name = "Nessun padre";
            _tmpLoc.NetworkId = NetworkId;
            _tmpLoc.ParentId = 0;
            _lstService.Add(_tmpLoc);
            ViewData["ServiceTypes"] = _lstService;
            int _networkId = oUserBLL.getUserNetwork(User.Identity.Name);
            if (_networkId == NetworkId)
                return View(oServiceBLL.getServiceTypeById(ServiceTypeId,NetworkId));
            else
                return RedirectToAction("AuthError",new { Error = "Mancano i permessi per l'azione richiesta"});
        }
        [Authorize]
        public ActionResult UpServiceTypeDetails(ClassRepository.ServiceTypesBase oUp)
        {
            Models.BLL.ServiceBLL oServiceBLL = new Models.BLL.ServiceBLL();
            List<ClassRepository.ServiceTypesBase> _lstService = oServiceBLL.getServiceTypesPerNetworkGrid(oUp.NetworkId);
            ClassRepository.ServiceTypesBase _tmpLoc = new ClassRepository.ServiceTypesBase();
            _tmpLoc.GUID = "";
            _tmpLoc.Id = 0;
            _tmpLoc.Name = "Nessun padre";
            _tmpLoc.NetworkId = oUp.NetworkId;
            _tmpLoc.ParentId = 0;
            _lstService.Add(_tmpLoc);
            ViewData["ServiceTypes"] = _lstService;
            NapoMVC.Models.BLL.UserBLL oUserBLL = new NapoMVC.Models.BLL.UserBLL();
            int _networkId = oUserBLL.getUserNetwork(User.Identity.Name);
            ViewBag.ins = "";
            ClassRepository.ResponseStatus oStatus = new ClassRepository.ResponseStatus();
            if (_networkId == oUp.NetworkId)
                return View("ServiceTypeDetails", oServiceBLL.setServiceType(oUp, out oStatus) );
            else
                return RedirectToAction("AuthError", new { Error = "Mancano i permessi per l'azione richiesta" });
        }

        [Authorize]
        public ActionResult AuthError(string Error)
        {
            return View(Error);
        }

        [Authorize]
        public ActionResult Services()
        {
            NapoMVC.Models.BLL.UserBLL oUserBLL = new NapoMVC.Models.BLL.UserBLL();
            Models.BLL.VATBLL oVATBLL = new Models.BLL.VATBLL();
            int _networkId = oUserBLL.getUserNetwork(User.Identity.Name);
            Models.BLL.ServiceBLL oServiceBLL = new Models.BLL.ServiceBLL();
            ViewData["SaleLists"] = oServiceBLL.getSalesListsPerNetwork(_networkId);
            ViewData["VAT"] = oVATBLL.getVatPerNetwork(_networkId);
            return View();
        }
        [Authorize]
        public ActionResult ServiceDetails(long ServiceId,int NetworkId)
        {
            return View();
        }
        [Authorize]
        public ActionResult Customers()
        {
            return View();
        }
        [Authorize]
        public ActionResult CustomerDetails(long CustomerId)
        {
            return View();
        }
        [Authorize]
        public ActionResult Cabins()
        {
            return View();
        }
        [Authorize]
        public ActionResult CabinDetails(long CabinId)
        {
            return View();
        }
        [Authorize]
        public ActionResult Suppliers()
        {
            return View();
        }
        [Authorize]
        public ActionResult SupplierDetails(long SupplierId)
        {
            return View();
        }
        [Authorize]
        public ActionResult ServiceTypes()
        {
            NapoMVC.Models.BLL.UserBLL oUserBLL = new NapoMVC.Models.BLL.UserBLL();
            int _networkId = oUserBLL.getUserNetwork(User.Identity.Name);
            Models.BLL.ServiceBLL oServiceBLL = new Models.BLL.ServiceBLL();
            ViewData["ServiceTypes"] = oServiceBLL.getServiceTypesPerNetworkGrid(_networkId);
            return View();
        }
        
        [Authorize]
        public ActionResult GeneralSettings()
        {
            return View();
        }
        [Authorize]
        public ActionResult Header()
        {
            return PartialView("_partialHeader");
        }
        public ActionResult ModalTypeinsert()
        {
            NapoMVC.Models.BLL.UserBLL oUserBLL = new NapoMVC.Models.BLL.UserBLL();
            int _networkId = oUserBLL.getUserNetwork(User.Identity.Name);
            return PartialView("~/Views/Shared/Partial/_PartialModalTypeAdd.cshtml", _networkId);
        }
    }
}