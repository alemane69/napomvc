﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using ClassRepository;
using NapoMVC.Models.BLL;
namespace NapoMVC.Controllers
{
    public class CRUDController : Controller
    {
        #region Employee
        [Authorize]
        public ActionResult Employees_Read(int NetworkId, [DataSourceRequest] DataSourceRequest request)
        {
            EmployeeBLL oBLL = new Models.BLL.EmployeeBLL();
            return Json(oBLL.getEmployeesInNetwork(NetworkId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult Employee_Update([DataSourceRequest] DataSourceRequest request, Employee EmployeeUp)
        {
            if (EmployeeUp != null && ModelState.IsValid)
            {
                EmployeeBLL oBLL = new Models.BLL.EmployeeBLL();
                oBLL.EmployeeUpdate(EmployeeUp);
            }
            return Json(new[] { EmployeeUp }.ToDataSourceResult(request, ModelState));
        }

        [Authorize]
        public ActionResult setEmployee(string JsonData)
        {
            EmployeeAPI oRet = new EmployeeAPI();
            Employee _employee = JsonConvert.DeserializeObject<Employee>(JsonData);
            EmployeeBLL oServiceBLL = new EmployeeBLL();
            UserBLL oUserBLL = new UserBLL();
            int _networkId = oUserBLL.getUserNetwork(User.Identity.Name);
            if (_networkId == _employee.NetworkId)
            {
                ResponseStatus _status = new ResponseStatus();
                oRet = oServiceBLL.setEmployee(_employee);
            }
            else
            {
                oRet.Status.Status = -100;
                oRet.Status.Error = "Errore id Network";
            }
            return Json(oRet, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult getUsersNonInemployee(int NetworkId)
        {
            UserBLL oBLL = new Models.BLL.UserBLL();
            List<UserNetwork> _lst = oBLL.getUserNetworkNonEmployee(NetworkId);
            return Json(_lst, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public ActionResult getUsersNonInemployeeWithSelect(int NetworkId,int UserId)
        {
            UserBLL oBLL = new Models.BLL.UserBLL();
            List<UserNetwork> _lst = oBLL.getUserNetworkNonEmployee(NetworkId,UserId);
            return Json(_lst, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Product
        [Authorize]
        public ActionResult Product_Read(int NetworkId, [DataSourceRequest] DataSourceRequest request)
        {
            ProductBLL oBLL = new Models.BLL.ProductBLL();
            return Json(oBLL.getProductsInNetwork(NetworkId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult Product_Update([DataSourceRequest] DataSourceRequest request, Product ProductUp)
        {
            if (ProductUp != null && ModelState.IsValid)
            {
                ProductBLL oBLL = new Models.BLL.ProductBLL();
                oBLL.ProductUpdate(ProductUp);
            }
            return Json(new[] { ProductUp }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Service
        [Authorize]
        public ActionResult Service_Read(int NetworkId, [DataSourceRequest] DataSourceRequest request)
        {
            ServiceBLL oBLL = new Models.BLL.ServiceBLL();
            return Json(oBLL.getServicesGridInNetwork(NetworkId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult Service_Update([DataSourceRequest] DataSourceRequest request, Service ServiceUp)
        {
            if (ServiceUp != null && ModelState.IsValid)
            {
                ServiceBLL oBLL = new Models.BLL.ServiceBLL();
                oBLL.ServiceUpdate(ServiceUp);
            }
            return Json(new[] { ServiceUp }.ToDataSourceResult(request, ModelState));
        }
        [Authorize]
        public ActionResult setService(string JsonData)
        {
            ServiceBaseResponse oRet = new ServiceBaseResponse();
            Service _serviceType = JsonConvert.DeserializeObject<Service>(JsonData);
            //_serviceType.ServiceType1.Id = _serviceType.type1;
            //_serviceType.ServiceType2.Id = _serviceType.type2;
            _serviceType.ServiceType = new ServiceTypeComplex();
            _serviceType.ServiceType.ServiceTypeChild.Id = _serviceType.type2;
            _serviceType.ServiceType.ServiceTypeParent.Id = _serviceType.type1;
            ServiceBLL oServiceBLL = new ServiceBLL();
            UserBLL oUserBLL = new UserBLL();
            int _networkId = oUserBLL.getUserNetwork(User.Identity.Name);
            if (_networkId == _serviceType.NetworkId)
            {
                ResponseStatus _status = new ResponseStatus();
                _serviceType = oServiceBLL.setService(_serviceType, out _status);
                oRet.Status = _status;
                oRet.Type = _serviceType;
            }
            else
            {
                oRet.Status.Status = -100;
                oRet.Status.Error = "Errore id Network";
            }
            return Json(oRet, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult ServiceTypes_Read(int NetworkId, [DataSourceRequest] DataSourceRequest request)
        {
            ServiceBLL oBLL = new ServiceBLL();
            return Json(oBLL.getServiceTypesPerNetworkGrid(NetworkId).ToDataSourceResult(request));
        }
        [Authorize]
        public ActionResult setServiceType(string JsonData)
        {
            ServiceTypeBaseResponse oRet = new ServiceTypeBaseResponse();
            ServiceTypesBase _serviceType = JsonConvert.DeserializeObject<ServiceTypesBase>(JsonData);
            ServiceBLL oServiceBLL = new ServiceBLL();
            UserBLL oUserBLL = new UserBLL();
            int _networkId = oUserBLL.getUserNetwork(User.Identity.Name);
            if (_networkId == _serviceType.NetworkId)
            {
                ResponseStatus _status = new ResponseStatus();
                _serviceType = oServiceBLL.setServiceType(_serviceType, out _status);
                oRet.Status = _status;
                oRet.Type = _serviceType;
            }
            else
            {
                oRet.Status.Status = -100;
                oRet.Status.Error = "Errore id Network";
            }
            return Json(oRet, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public ActionResult getParentServices(int NetworkId)
        {
            Models.BLL.ServiceBLL oServiceBLL = new Models.BLL.ServiceBLL();
            List<ServiceTypesBase> _lst = oServiceBLL.getServiceTypesPerNetworkGrid(NetworkId).Where(x=>x.ParentId==x.Id).ToList();
            return Json(_lst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getServices(int NetworkId)
        {
            Models.BLL.ServiceBLL oServiceBLL = new Models.BLL.ServiceBLL();
            List<ServiceTypesBase> _lst = oServiceBLL.getServiceTypesPerNetworkGrid(NetworkId);
            return Json(_lst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getChildrenServices(int NetworkId,int ParentId)
        {
            Models.BLL.ServiceBLL oServiceBLL = new Models.BLL.ServiceBLL();
            List<ServiceTypesBase> _lst = oServiceBLL.getServiceTypesPerNetworkGrid(NetworkId);
            var q = from x in _lst where x.ParentId == ParentId && x.Id != x.ParentId select x;
            _lst = q.ToList();
            return Json(_lst, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Customer
        [Authorize]
        public ActionResult Customer_Read(int NetworkId, [DataSourceRequest] DataSourceRequest request)
        {
            CustomerBLL oBLL = new Models.BLL.CustomerBLL();
            return Json(oBLL.getCustomersInNetwork(NetworkId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult Customer_Update([DataSourceRequest] DataSourceRequest request, Customer CustomerUp)
        {
            if (CustomerUp != null && ModelState.IsValid)
            {
                CustomerBLL oBLL = new Models.BLL.CustomerBLL();
                oBLL.CustomerUpdate(CustomerUp);
            }
            return Json(new[] { CustomerUp }.ToDataSourceResult(request, ModelState));
        }
        #endregion
        #region Cabin
        [Authorize]
        public ActionResult Cabins_Read(int NetworkId, [DataSourceRequest] DataSourceRequest request)
        {
            CabinBLL oBLL = new Models.BLL.CabinBLL();
            return Json(oBLL.getCabinsInNetwork(NetworkId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult Cabin_Update([DataSourceRequest] DataSourceRequest request, Cabin CabinUp)
        {
            if (CabinUp != null && ModelState.IsValid)
            {
                CabinBLL oBLL = new Models.BLL.CabinBLL();
                oBLL.CabinUpdate(CabinUp);
            }
            return Json(new[] { CabinUp }.ToDataSourceResult(request, ModelState));
        }
        #endregion
        #region Supplier
        [Authorize]
        public ActionResult Suppliers_Read(int NetworkId, [DataSourceRequest] DataSourceRequest request)
        {
            SupplierBLL oBLL = new Models.BLL.SupplierBLL();
            return Json(oBLL.getSuppliersInNetwork(NetworkId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult Supplier_Update([DataSourceRequest] DataSourceRequest request, Supplier SupplierUp)
        {
            if (SupplierUp != null && ModelState.IsValid)
            {
                SupplierBLL oBLL = new Models.BLL.SupplierBLL();
                oBLL.SupplierUpdate(SupplierUp);
            }
            return Json(new[] { SupplierUp }.ToDataSourceResult(request, ModelState));
        }
        [Authorize]
        public ActionResult getVAT(int NetworkId)
        {
            Models.BLL.VATBLL oBLL = new Models.BLL.VATBLL();
            List<VAT> _lst = oBLL.getVatPerNetwork(NetworkId);
            return Json(_lst, JsonRequestBehavior.AllowGet);
        }
        #endregion


    }
}